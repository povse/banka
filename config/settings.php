<?php

const DBHOST = "localhost";
const DBUSER = "bankdemo";
const DBPASSWORD = "";
const DBNAME = "bankdemo";
const TRANSACTIONS_START_DATE = "2018-01-01";
const TRANSACTIONS_END_DATE = "2018-06-30";
const NUMBER_OF_TRANSACTIONS_PER_USER = 99;
