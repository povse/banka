<?php

class Bank {
	
  private $dbCon;

  public function __construct($dbHost, $dbUser, $dbPassword, $dbName) {
  	$this->dbCon = new mysqli($dbHost, $dbUser, $dbPassword, $dbName);
  	if ($this->dbCon->connect_errno) {
  	  printf("Connect failed: %s\n", $this->dbCon->connect_error);
  	  exit();
  	}
  }

  public function __destruct() {
  	$this->dbCon->close();
  }

  public function randomize() {
  	for($i=0; $i<=5; $i++) {
  	  $this->createUserWithTransactions();
  	}
  }

  public function balance() {
  	$query = "SELECT (
  				(select sum(deposit) from transactions where transactions.user_id = users.id) - 
  				(select sum(withdraw) from transactions where transactions.user_id = users.id)
  			 )as balance, name  FROM users";

  	$result = $this->dbCon->query($query);

  	$users = [];
  	while($user = $result->fetch_array(MYSQLI_ASSOC)) {
  	  array_push(
  	  	$users,
  	  	[
  	  	  "name"    => $user["name"],
  	  	  "balance" => $user["balance"]
  	  	]
  	  );
  	}

  	return $users;
  }


  public function transactions() {
  	return $this->getTransactions();
  }

  public function dailyTransactions($month) {
  	return $this->getTransactions($month);
  }

  public function negativeBalance() {
  	return $this->getTransactions(false, true);
  }

  /*
  * Getters
  */

  private function getTransactions($forMonth = false, $withdrawOnly = false) {

  	$where = $forMonth ? sprintf(" AND DATE_FORMAT(transactions.date, \"%%c\") = %d", $forMonth) : "";
  	$whereWithdraw = $withdrawOnly ? " AND transactions.withdraw > 0" : "";

  	$query = "SELECT users.*, transactions.*, DATE_FORMAT(transactions.date, \"%c\") as month 
  				FROM users JOIN transactions ON transactions.user_id = users.id 
  			  WHERE 1=1 $where $whereWithdraw	
  			  order by month desc";
  	$result = $this->dbCon->query($query);
  	
  	$userTransactions = [];
  	while($transaction = $result->fetch_array(MYSQLI_ASSOC)) {
  	  
  	  if (!isset($userTransactions[$transaction["user_id"]])) {
  	  	$userTransactions[$transaction["user_id"]] = [
  	  		"name" => $transaction["name"],
  	  		"months" => []
  	  	];
  	  } 

  	  if (!isset($userTransactions[$transaction["user_id"]]["months"][$transaction["month"]])) {
  	  	$userTransactions[$transaction["user_id"]]["months"][$transaction["month"]] = [];
  	  }

  	  array_push(
  	  	$userTransactions[$transaction["user_id"]]["months"][$transaction["month"]],
  	  	[
  	  	  "id"       => $transaction["id"],
  	  	  "date"     => $transaction["date"],
  	  	  "deposit"  => $transaction["deposit"],
  	  	  "withdraw" => $transaction["withdraw"]
  	  	]
  	  );
  	}

  	return $userTransactions;
  }

  /*
  * Generators
  */

  private function createUserWithTransactions() {
  	$user = $this->getRandomUser();
  	$this->dbCon->query(
  	  sprintf(
  	    "INSERT INTO users(name, birth_date) VALUES('%s', '%s')",
  	    sprintf("%s %s", $user["firstName"], $user["lastName"]),
  	    $user["birthDate"]->format("Y-m-d")
  	  )
  	);
  	$this->createTransactionsForUser($this->dbCon->insert_id);
  }

  private function createTransactionsForUser($userId) {
  	for($i=0; $i<=NUMBER_OF_TRANSACTIONS_PER_USER; $i++) {
  		$this->generateRandomTransaction($userId);
  	}
  }

  private function generateRandomTransaction($userId) {
  	$depositOrWithdraw = mt_rand(0,1);
	$this->dbCon->query(
  	  sprintf(
  	    "INSERT INTO transactions(user_id, date, deposit, withdraw) VALUES(%d, '%s', %d, %d)",
  	    $userId,
  	    $this->getRandomDate(TRANSACTIONS_START_DATE, TRANSACTIONS_END_DATE)->format("Y-m-d h:i:s"),
  	    $depositOrWithdraw == 0 ? rand(10, 999) : 0,
  	    $depositOrWithdraw == 1 ? rand(10, 999) : 0
  	  )
  	);
  }

  private function getRandomUser() {
  	$firstNames = ["Marjana", "Silva", "Ljuba", "Radana", "Rajmund", "Brigita", "Ožbalt"];
  	$lastNames = ["Horvat", "Strnad", "Kovač", "Vlašič", "Medved", "Zupan"];

  	return [
  		"firstName" => $firstNames[array_rand($firstNames, 1)],
  		"lastName" => $lastNames[array_rand($lastNames, 1)],
  		"birthDate" => $this->getRandomDate("1970-01-01", "1995-12-31")
  	];
  }

  private function getRandomDate($startDate, $endDate) {
  	$startDateTimestamp = new \DateTime($startDate);
  	$endDateTimestamp = new \DateTime($endDate);

  	$randomDate = new \DateTime();
  	$randomDate->setTimestamp(
  		mt_rand($startDateTimestamp->getTimestamp(), $endDateTimestamp->getTimestamp())
  	);
  	return $randomDate;
  }

}
