<?php 
require_once "config/settings.php";
require_once 'models/Bank.php';

if(count($argv) < 2) {
  print "Missing arguments\n";
  exit();
}

switch ($argv[1]) {
  case "createTables":
    createTables();
    break;
  case "randomize":
  	$bank = new Bank(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  	$bank->randomize();
  	break;
  case "balance":
  	balance();
  	break;
  case "transactions":
    transactions();
    break;
  case "dailyTransactions":
  	if(!isset($argv[2]) || (int)$argv[2] > 12 || (int)$argv[2] < 1) {
  	  print "Missing or wrong month param\n";
  	  exit();
  	}
  	dailyTransactions($argv[2]);
  	break;
  case "negativeBalance":
  	negativeBalance(false, true);
  	break;
}

/*
* Database creation
*/

function createTables() {
  $conn = new mysqli(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  if ($conn->connect_error) {
    printf("Connect failed: %s\n", $conn->connect_error);
  	exit();
  }

  $conn->query(
  	"CREATE TABLE `transactions` (
	  `id` int(11) NOT NULL,
	  `user_id` int(11) NOT NULL,
	  `date` datetime NOT NULL,
	  `deposit` decimal(10,0) NOT NULL,
	  `withdraw` decimal(10,0) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
  );
  $conn->query("ALTER TABLE `transactions`
	  ADD PRIMARY KEY (`id`);");
  $conn->query("ALTER TABLE `transactions`
  		MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;");

  $conn->query(
  	"CREATE TABLE `users` (
	  `id` int(11) NOT NULL,
	  `name` varchar(255) NOT NULL,
	  `birth_date` date NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
  );
  $conn->query("ALTER TABLE `users`
  		ADD PRIMARY KEY (`id`);");
  $conn->query("ALTER TABLE `users`
  		MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;");
}

/*
* Display functions
*/

function balance() {
  $bank = new Bank(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  $users = $bank->balance();
  print("  -------- Balance --------\n");
  printf("| User name | Balance |\n");
  print("-----------------------\n");
  foreach ($users as $user) {
    printf("| %s | %s | \n", $user["name"], $user["balance"]);
  }
  print("-----------------------\n");
}

function transactions() {
  print("  -------- Transactions --------\n");
  $bank = new Bank(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  renderTransactions($bank->transactions());
}

function dailyTransactions($forMonth) {
  print("  -------- dailyTransactions --------\n");
  $bank = new Bank(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  renderTransactions($bank->dailyTransactions($forMonth));
}

function negativeBalance() {
  print("  -------- negativeBalance --------\n");
  $bank = new Bank(DBHOST, DBUSER, DBPASSWORD, DBNAME);
  renderTransactions($bank->negativeBalance());
}

function renderTransactions($users) {
  foreach ($users as $user) {
  	foreach($user["months"] as $month => $transactions) {
  	  printf("\nUser: %s - Month: %s\n", $user["name"], $month);
  	  print("  | id | date | deposit | withdraw | \n");
  	  print("  -----------------------------------\n");
  	  foreach ($transactions as $transaction) {
  	  	printf("  | %d | %s | %d | %d | \n", $transaction["id"], $transaction["date"], $transaction["deposit"], $transaction["withdraw"]);
  	  }
  	  print("  -----------------------------------\n");
  	}	
  }
}
